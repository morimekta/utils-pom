Shared POM for Morimekta Utilities
==================================

[![Morimekta](https://img.shields.io/static/v1?label=morimekta.net&message=utils&color=informational)](https://morimekta.net/utils/)
[![License](https://img.shields.io/static/v1?label=license&message=apache%202.0&color=informational)](https://apache.org/licenses/LICENSE-2.0)  
Master pom to reduce boilerplate between the various
utilities project under the net.morimekta.utils package.

# Releasing Utilities

Run the maven versions plugin to see what has been updated of dependencies and
plugins. See if updates should be done. Usually it's better to depend on
newer versions, as you may drag in older versions into other projects that
misses features or has specific bugs.

```bash
mvn versions:display-parent-updates
mvn versions:display-plugin-updates
mvn versions:display-dependency-updates
git log --oneline v$(git tag | semver --max)..
```

## Making The Release.

Or for single-module (simple) utility projects:

```bash
# Do the maven release:
mvn release:prepare
mvn release:perform
mvn release:clean
git fetch origin
```

Or for multi-module projects:

```bash
mvn -Pall release:prepare
mvn -Plib release:perform
mvn -Pall release:clean
git fetch origin
```

If the artifacts found at the
[Nexus Repository Manager](https://oss.sonatype.org/#stagingRepositories)
are correct, you're ready to make the release. If not a git hard rollback is
needed (to remove release version, tag and commits).