#!/bin/bash

GROUP_ID=$(xmllint --xpath '/*[local-name()="project"]/*[local-name()="groupId"]/text()' pom.xml)
if [[ -z "$GROUP_ID" ]]; then
  GROUP_ID=$(xmllint --xpath '/*[local-name()="project"]/*[local-name()="parent"]/*[local-name()="groupId"]/text()' pom.xml)
fi
ARTIFACT=$(xmllint --xpath '/*[local-name()="project"]/*[local-name()="artifactId"]/text()' pom.xml)
VERSION=$(git tag -l --sort=-creatordate | grep '^v[0-9]\+[.][0-9]\+[.][0-9]\+' | head -n 1 | sed 's/^v//')
TYPE=$(xmllint --xpath '/*[local-name()="project"]/*[local-name()="packaging"]/text()' pom.xml)
PACKAGE=$(echo $GROUP_ID | sed 's:\.:/:g')
TYPE_NOT_JAR=:${TYPE}
TYPE_NOT_JAR=${TYPE_NOT_JAR/:jar/}

TRY=1

while [[ true ]]
do
  rm -rf ~/.m2/repository/${PACKAGE}/${ARTIFACT}
  echo -e "\033[32mDownloading: ${GROUP_ID}:${ARTIFACT}:${VERSION}${TYPE_NOT_JAR}\033[00m"
  echo -e " -- Attempt ${TRY}"
  mvn -q download:artifact \
      -DgroupId=${GROUP_ID} \
      -DartifactId=${ARTIFACT} \
      -Dversion=${VERSION} \
      -Dtype=${TYPE} > /dev/null 2>&1 \
  && echo -e "\033[32mSuccess!\033[00m" \
  && echo -e " -- $(date --iso-8601=seconds | sed 's/+.*//')" \
  && exit 0

  echo -e '\033[31mFailed download...\033[00m'
  echo -e " -- $(date --iso-8601=seconds | sed 's/+.*//')"
  sleep 60
  TRY=$(echo ${TRY} + 1 | bc)
  echo
done
